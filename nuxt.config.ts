// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  css: ['@/assets/css/reset.css'],

  modules: ['@nuxtjs/google-fonts', "@nuxt/image"],

  googleFonts: {
    families: {
      Figtree: [600, 800]
    }
  }
})
